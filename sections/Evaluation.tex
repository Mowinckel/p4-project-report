%!TEX root = ../master.tex
\chapter{Evaluation}\label{ch:eval}
The prototype was evaluated through three distinctive tests. Firstly, the prototype was tested for its intuitiveness through a mental model elicitation test. Secondly, the prototype's capability was evaluated and measured through a designed technical test. Lastly, the aspect of learning were evaluated on the target group through a music knowledge test.

\section{Technical Test}
\subsection{Procedure}
The technical test was designed to cover all functions of the program. The functions were tested multiple times, so the success rate could be calculated from the results. 
\par 

The test was conducted by two members of the group as an internal test. One noted down the fail or success of each task, while the other interacted with the system and therefore took up the role of a fictional user. The "user" was a member of the group who has not worked with the latest version of the program and therefore was not very familiar with the interaction with the system.
The results were collected in Google Sheets with a simple coding which linked the results back to its original questions (see Appendix A6).
As an extra tool, a timer was used to note down the possible delays that might have occurred during testing of some of the functions. The timer was not part of the tested system, so that all linked delays were avoided.
\par 

The test had a few parts focusing on different aspects of the program - overall functionality, visuals, audio, tasks/actions, delay and light/LED. The test had 15 main questions and those usually also contained some number of sub-questions. With only one exception, all questions could have been answered with a yes or no. The exception was for the measured delay times under questions 14a, 14b and 14c.

\subsection{Results and Analysis}
One of the success criteria for the final evaluation is:\\
\textbf{Producing a real-life synthesizer should be understood by using common waveform synthesizer techniques}
\par 
This success criterion not only included the knowledge that was obtained from background research, but also included the efficiency of the prototype. This was done by conducting the technical test and calculating the average score and check if at least half of its functions work as desired. 
\par 

The overall success of the test was 82\%, which would indicate that the prototype is efficient in performing some of the different task without too many errors.
\par 

During the development, a few bugs occurred while holding down the note buttons for different durations of time. This was due to the functionality of adding a longer note if you press it for a longer or shorter period of time. The average success rate of pressing the buttons for less than half a second was 80\%. It was a success, if a short note was spawned in the visual interface. However, it is more likely, the user will press for a longer time than that, as the buttons are hard to press and therefore the user needs to use a bit more force. However, the average success rate for pressing the note button for one second was only 56\%, pressing for three seconds had almost a 100\% success rate, specifically 96\%. Talking about the average success rate might be misleading when explaining question 1b, as there seems to be significantly different success percentage results, going from a 0\% fail to a 100\% success. Questions 1a and 1c had more or less similar success rate, therefore those results are more likely to be correct.
\par 

Controls for waveforms, navigation and chords all had a 100\% success, which add to the claim: that the encountered problems with spawning the notes had a relation to the timing function of the program.
\par 

Another function of the program, that does not seem to work properly is the undo button. Questions 5a, 5b and 5d all had a success rate above 70\% which is acceptable, but 5c only had a low success of 30\%. Through this result, it became apparent that a function for erasing the last added beat in the visual representation was not implemented. The undo button needed to be further tested and remade to increase its overall success rate of 67\%.
\par 

Another notable outcome of the test was the realisation that a clear indication of which ring is currently selected have not yet been implemented. With a success rate of 40\%, it was discovered that there is a slight delay up to one second between playing the audio and the visual representation of the note being played. The note should be played when it passes an invisible collider at the top of the screen, which also triggers a small visual effect. This led to a decision to further improve on the speed of the program, so that those audio delays can be avoided.
\par 

Throughout the testing, a few bugs were reported. The previously mentioned delay was most likely with correlation with the audio playing at wrong places, which was reported during the first set of tasks focusing on spawning the notes. Other bugs worthy of mention would be spawning two of the same note. This might have been caused by the system registering a single button press as multiple presses. The reason behind this occurrence could be that not enough pressure was applied to the button. The full list of bugs, together with the specific task during which they occurred can be seen below.

\begin{itemize}
\item Sound played at a wrong place (1a)
\item There were two waveforms spawned on one planet (5aa)
\item There are sometimes two planets spawned when overriding (5b)
\item The planet was not there when overridden and returned (5b)
\item Sometimes two planets are spawned instead of one (5c)
\item Sometimes the planet is removed even if it should not have been (5d)
\end{itemize}


\section{User Evaluation}
\subsection{Procedure}
The evaluation was handled by three members, who were also recording the evaluation. The tools used were the prototype, containing an Arduino and its circuit, a laptop, speakers, the script, questionnaires and consent forms. The evaluation was conducted at Aalborg University - Create and Basis campuses. 
\par 

For the evaluation, a script for the procedure was made. First and foremost, the interviewer introduced himself to the participant and explained what the project was about. The participant would sign a consent form if they wished to participate and afterwards the procedure of the test would be explained. Next, the evaluation began and after testing, they were answering a questionnaire. Before ending the evaluation, the participant was asked if he or she wanted to try the program again. After the answer was given, the participant was thanked for helping.
\par 

Two different programs were tested on two different sample groups. The program for our prototype and a similar one, named Seaquence (see Figure \ref{fig:seaquence}. The reason for choosing Seaquence was because it is very similar to our program in that it tries to make it easy to produce music. The participant did not know which program was the "treatment", meaning that the participant was being blind tested. The test started by getting the participant to know the program that was being tested by playing with it and learn from it. After ten minutes, the participant would take a test containing 14 questions related to music (see Appendix A8), in order to test what they have learned. Nine of the questions were of multiple choice, with a set of pre-made answers and four of them Likert scale. The reason for utilising Likert scale was to see how sure they were of their answers. The last question, which was asked in person, was binary in nature, as it only had two outcomes which, in this case, were yes or no.

\begin{figure} [!ht]
	\centering
	\includegraphics[scale=0.7]{seaquence.png}
	\caption{Screenshot of the web-browser game Seaquence.}
	\label{fig:seaquence}
\end{figure}

\subsection{Plan and statistic methods}
For the evaluation two hypotheses related to each success criteria were made. A null hypothesis, which we wish to reject, and an alternative hypothesis, which will be accepted if the null hypothesis is rejected. For each set of hypotheses, a set of assumptions based on relevant theory, which will ensure that the results are valid, were made.
The evaluation is one tailed, since each hypothesis only wish to cover "one direction", either \textit{more than} or \textit{less than}, as can be seen in the hypotheses. The adopted concept for the test is named a two sample test. We wish to test two samples from the same population. The participants will be randomized by making a list of all the groups and number them. Afterwards a number will be randomly generated and the group corresponding to that number will be the first group to be asked if they want to participate. This procedure will be repeated until all of the groups are prioritised. This is not true randomization since it is not done for each participant but done in groups instead. Furthermore, each question and answer from the questionnaire will be randomized, to decrease that the chance of getting a similar questionnaire as the other participants. Participants that already have knowledge about music theory and already know the program "Seaquence" were blocked. Before participating, each participant was  asked what their knowledge of music and if they knew of "Seaquence". The explanatory variable is the two programs we are testing. "Seaquence" and our program "Solar Music". The level of measurement is nominal, since it is categorical and is a name/label. The response variable is the total score of correct answers, which is of ratio level, since the variable can be divided/multiplied, added/subtracted and compared to see if it is equal, greater or less than another variable.

\subsection{Analysis and Results}
Two of the success criteria and their belonging hypotheses and assumptions are listed and analysed.

\textbf{The user should be more encouraged to create music or explore electronic music after using the prototype}
\par 
H$_{\text{0}}$: \textit{Less than half of the participants are more encouraged to create music or explore electronic music.}
\\
H$_{\text{A}}$: \textit{More than half of the participants are more encouraged to create music or explore electronic music.}
\par 

Assumption:
\begin{enumerate}
\item We assume the variables are nominal-level
\\
This assumption is related to whether the variables are of nominal-level. They are of nominal level, because the values  can be categorised into their own label. This is because of the variables used are "Yes" and "No", "Seaquence" and "Solar Music". Therefore, this assumption is verified

\item We assume the observations are independent
\\
To find out if the observations are independent, chi-squared test is used. The testing looks at if the variables are associated with each other, while they are taken from the same population.
\par
\end{enumerate}

The results from the evaluation as shown in Table \ref{yes_no_table} shows that "Seaquence and No" are dependent, as with "Solar Music and Yes". However "Seaquence and Yes" and "Solar Music and No" are independent as the values associated with the two are below the expected frequencies.
From the Table \ref{O_Etable} it can also be seen that there is almost the same amount of participants that answered "yes" to trying again as well as "no" in both testing groups. However there is a small difference in the two groups, as there is slightly more participants who would continue testing the Solar Music. The chi-squared is calculated using R:
\par

\[\chi^2 = 0.1357\]
\[qchisq(0.95,1)\]
\[[1]3.841459\]
\par
This value can also be obtained by adding the elements of the last column of Table \ref{O_Etable} together.
\par
As the chi-squared is less than the relevant critical value of \(p=3.841459\), the null hypothesis is accepted. Therefore this is a Type II error, since the data shows that the number of respondents who said "yes" is higher for "Solar Music" than the responses of "no". However the frequencies are very close to the expected frequencies, which is why it is not possible to say anything conclusive. 
Using R, the chi-square is performed. 
\par

\[Pearson's Chi-squared test\]
\[data:  responses\]
\[\chi-squared = 0.13575, df = 1, p-value = 0.7125\]
\par

The p-value is higher than the significance level of 0.05 which means that the null hypothesis can not be rejected. Therefore in order to correct the Type II error, one could use power. This means to increase the sample size or use a better measuring for the hypothesis. Other possibilities could be to increase the significance level, which in turn increases the risk of making a Type I error. 
\par

\begin{table}[]
\centering
\label{yes_no_table}
\begin{tabular}{|l|l|l|l|}
\hline
Observed      & Yes & No & Row totals \\ \hline
Seaquence     & 8   & 7  & 15         \\ \hline
Solar Music   & 9   & 6  & 15         \\ \hline
Column totals & 17  & 13 & 30         \\ \hline
\end{tabular}
\caption{Responses from the last question asked during the evaluation}
\end{table}

\begin{table}[]
\centering
\label{O_Etable}
\begin{tabular}{llll|l|}
\hline
\multicolumn{1}{|l|}{}                    & \multicolumn{1}{l|}{O} & \multicolumn{1}{l|}{E}   & (O-E)\textasciicircum 2 & (O-E)\textasciicircum 2/E \\ \hline
\multicolumn{1}{|l|}{Seaquence and Yes}   & \multicolumn{1}{l|}{8} & \multicolumn{1}{l|}{8.5} & 0.25                    & 0.02941176471             \\ \hline
\multicolumn{1}{|l|}{Seaquence and No}    & \multicolumn{1}{l|}{7} & \multicolumn{1}{l|}{6.5} & 0.25                    & 0.03846153846             \\ \hline
\multicolumn{1}{|l|}{Solar Music and Yes} & \multicolumn{1}{l|}{9} & \multicolumn{1}{l|}{8.5} & 0.25                    & 0.02941176471             \\ \hline
\multicolumn{1}{|l|}{Solar Music and No}  & \multicolumn{1}{l|}{6} & \multicolumn{1}{l|}{6.5} & 0.25                    & 0.03846153846             \\ \hline
                                          &                        &                          &                         & 0.1357466063              \\ \cline{5-5} 
\end{tabular}
\caption{The observed and expected values}
\end{table}

The previously mentioned success criterion had a subpoint saying "\textbf{Understanding the system(basic electronic music)}". Therefore we made the hypotheses:
\par

H$_{\text{0}}$: \textit{More participants testing Seaquence will learn more music theory than participants testing Solar Music}
\\
H$_{\text{A}}$: \textit{More participants testing Solar Music will learn more music theory than participants testing Seaquence}
\par

Assumption:
\begin{enumerate}
\item We assume the response variables are interval ratio-level
\\
Since we will look at the amount of correct and incorrect answers and this is a numerical value, basic mathematics can be applied. This means it is of ratio-level. Therefore this assumption is confirmed.
\item We assume the observations are independent
\\
Each participant was tested individually and only once, so therefore the observations are independent. The assumption is confirmed.
\end{enumerate}
\par

\begin{table}[!ht]
\centering
\label{table:contingency_table_1}
\begin{tabular}{|l|l|l|l|l|}
\hline
                      	& O  & E    & (O-E)\textasciicircum 2 & (O-E)\textasciicircum 2/E \\ \hline
Correct and Solar     	& 96 & 84.5 & 132.25                  & 1.565088757               \\ \hline
Correct and Seaquence 	& 73 & 84.5 & 132.35                  & 1.565088757               \\ \hline
Incorrect and Solar   	& 72 & 83.5 & 132.25                  & 1.583832335               \\ \hline
Incorrect and Seaquence	& 95 & 83.5 & 132.25                  & 1.583832335               \\ \hline
\end{tabular}
\caption{Observed and expected results}
\end{table}

\par

If we put the observed data into a contingency table (see \ref{table:contingency_table_1}) and calculate the expected frequencies of each outcome, it seems that “Correct and Solar” are dependent of each other and “Incorrect and Seaquence” are dependent as well. Furthermore, it looks like "Correct and Seaquence” and "Incorrect and Solar" are independent, since the expected is more than the observed. The expected frequencies show that there is a positive correlation between getting correct answers in the music theory questionnaire and having tried out the “Solar music” program. It also shows that there is a negative correlation between “Sequence” and getting correct answers, which means that “Solar Music” is better to teach music theory related to the content used in the questionnaire. In order to check if the findings are of any significance, the chi-squared value of the test statistic is checked with the relevant critical value.
\par
\[\chi^2 = 6.2978\]
\[qchisq(0.95,1)\]
\[[1]3.841459\]
\par
This value can also be obtained by adding the elements of the last column of Table \ref{table:contingency_table_1} together.
\par
Since the chi-squared value 6.2978 is greater than the relevant critical value \(p=3.41459\), the null hypothesis \(H_0\) is rejected and the alternative hypothesis \(H_a\) is accepted for now.
Using R, a chi-square test is performed. 
\par
\[> chisq.test(count, correct=FALSE)\]
\[Pearson's Chi-squared test\]
\[data:  count\]
\[\chi-squared = 6.2978, df = 1, p-value = 0.01209\]
\par
The p-value is less than the significance level of 0.05, which also means that the null hypothesis can be rejected. Rejecting the null hypothesis corresponds to the gathered data, since more participants got correct answers in the questionnaire when testing “Solar Music” compared to when testing “Seaquence” (see \ref{table:contingency_table_2}). 
\par

\begin{table}[!ht]
\centering
\label{table:contingency_table_2}
\begin{tabular}{|l|l|l|l|}
\hline
              & Solar Music & Seaquence & Row Totals \\ \hline
Correct       & 96          & 73        & 169        \\ \hline
Incorrect     & 72          & 95        & 167        \\ \hline
Column Totals & 168         & 168       & 336        \\ \hline
\end{tabular}
\caption{Amount of correct and incorrect answers from the questionnaire}
\end{table}

\subsubsection{Likert Scale}
Last four questions in the questionnaire were statements, which the user could agree or disagree with on a scale from one to six. Please note, that the correct answers for the first three is to agree, and disagree for the last question. 
\par 

\begin{figure} [!ht]
	\centering
	\includegraphics[scale=0.7]{solarQ10.PNG}
	\caption{Question 10 answers for Solar.}
	\label{fig:seaquenceQ10}
\end{figure}

\begin{figure} [!ht]
	\centering
	\includegraphics[scale=0.7]{seaquenceQ10.PNG}
	\caption{Question 10 answers for Seaquence.}
	\label{fig:seaquenceQ10}
\end{figure}

\textbf{Question 10: "How much do you agree with the following statement: C note sounds the same on different channels."}

By looking at the distribution of the answers for the question, we can see, that participants were unsure about their answers (see Figure \ref{fig:solarQ10}). No one was strongly agreeing or disagreeing  with the statement, which shows, that the participants most likely did not understand that different rings are the different channels. The majority of the answers are plotted in the middle area, leaning towards the correct answer. Also, the results were generally very similar with eight people leaning towards the correct answer and seven towards the incorrect.

The participants who were part of the control group were a bit more sure about their answers (see Figure \ref{fig:seaquenceQ10}). The distribution is a bit more varied and also leaning towards the correct answer. 

\begin{figure} [!ht]
	\centering
	\includegraphics[scale=0.7]{solarQ11.PNG}
	\caption{Question 11 answers for our prototype.}
	\label{fig:solarQ11}
\end{figure}

\begin{figure} [!ht]
	\centering
	\includegraphics[scale=0.7]{seaquenceQ11.PNG}
	\caption{Question 11 answers for Seaquence.}
	\label{fig:seaquenceQ11}
\end{figure}

\textbf{Question 11: "How much do you agree with the following statement: It's easy to make a melody with the tested tool."}

This question was designed to find out how easy the tested tool was to work with. The answers were very varied, but seemingly the participants inclined towards "no" (see Figure \ref{fig:solarQ11}). One third of the participants answered with unsure no. This might be because they were trying to avoid disagreeing and go with a more subtle option.

Interestingly, participants who were part of the control group were mostly avoiding the middle unsure ground (see Figure \ref{fig:seaquenceQ11}). The majority strongly agreed or somewhat agreed with the statement and only 40\% of responses were scattered in the disagreeing section. 

\begin{figure}[!ht]
	\centering
	\includegraphics[scale=0.7]{solarQ12.PNG}
	\caption{Question 12 answers for our prototype.}
	\label{fig:solarQ12}
\end{figure}

\begin{figure}[!ht]
	\centering
	\includegraphics[scale=0.7]{seaquenceQ12.PNG}
	\caption{Question 12 answers for Seaquence.}
	\label{fig:seaquenceQ12}
\end{figure}

\textbf{Question 12: "How much do you agree with the following statement: There is a difference when playing an identical note with different waveforms."}

Only around 27\% of the treatment group answered incorrectly (see Figure \ref{fig:solarQ12}), which shows, that this information was more or less sufficiently taught or shown in the prototype. Almost 47\% of Seaquence users were very sure about the answer, which would suggest, that the program is even better at showing this aspect (see Figure \ref{fig:seaquenceQ12}). However, the three incorrect answers were strongly disagreeing, which would suggest, that maybe something important was misunderstood.

\begin{figure} [!ht]
	\centering
	\includegraphics[scale=0.7]{solarQ13.PNG}
	\caption{Question 13 answers for our prototype.}
	\label{fig:solarQ13}
\end{figure}

\begin{figure} [!ht]
	\centering
	\includegraphics[scale=0.7]{seaquenceQ13.PNG}
	\caption{Question 13 answers for Seaquence.}
	\label{fig:seaquenceQ13}
\end{figure}
\clearpage

\textbf{Question 13: "How much do you agree with the following statement: A note can have more that one waveform at a time."}

This question was a bit tricky because of the wording, as unlike the others, the participants would have to disagree with the statement. No participant strongly disagreed with the statement, however majority of the participants did answer correctly, which could suggest, that they were very unsure of the answer to the question. On the other hand, over 73\% of the participants answered incorrectly. This clearly shows that our visualisation of notes having only a single waveform attached helps more at teaching this fact to the participants. 



\section{Discussion of Evaluation}
This chapter will discuss sources of error that might have had an affect on the gathered results.
During the evaluation the participant was asked whether they had some prior musical knowledge. However, it may have been possible that the participant misinterpreted the question by thinking if they had ever played an instrument instead of knowing musical theory or terminology. 
\\
While the evaluation test was being conducted, the plan of how to analyse the data was made. This resulted in redoing the plan of the analysis afterwards in order to be able to analyse the data properly. Also, a new set of hypotheses was added after conducting the evaluation.
\\
According to the results our program is better at teaching musical knowledge than the compared program "Seaquence". The positive outcome might have derived from the fact that the questionnaire was made in relation to our program, since this was the main focus. "Seaquence" might not aim to teach musical knowledge in the same manner as our program.
\\
More participants might have been necessary for the evaluation, since one of our null hypotheses was not rejected and therefore resulted in a Type II error due to having a p-value too high. This could also be due to not having truly randomized the samples. Only the groups of which the participants are assigned to were randomized. This could be done if we collected all the participants prior and then randomized them truly, so each and every participant would have equal amount of chance of being tested.
\\
Another source of error might have been the very last question asked during the evaluation. The participants were asked if they could ever see themselves playing with our program again. The participants might have shown emphasis and not answered truly honest. This problem could be countered if the question was part of the questionnaire instead of it being asked face to face.
\\
Some of the participants said that they had a hard time understanding the questions. This could mean that some of the questions were answered incorrectly simply because of the phrasing of the question even though the user might have known the answer if it had been phrased better.
\\
Using the Likert scale also has its own disadvantages. For example, the way the test is intended is that the difference between two sequential answers is the same. However, the participant might interpret this intention differently and place more value on the difference between, for example, "3" and "4" than "5" and "6". This, in turn, may lead to distorted results in the analysis.
\\
The prototype sometimes did not respond as correctly as we would have liked it to. For example it required the user to hold down the button on the interface for at least 0.5 seconds for it to even register. This sometimes led to frustration in our participants and could result in an overall more negative experience. This could be solved if we had used more time to test our program and therefore correct these errors before testing.
\\
The two tests were conducted by different people so the exact same procedure might not have been followed which could result in varying result between the tests. This could be solved if both tests were conducted by the same people.
\\
There were also two of our participants that knew certain members from our group. Even though the members were not a part of the testing it could still have influenced the results. An easy way to solve this would be to not test on them and test on people who would be less biased.
\\
After conducting the test and returning to their group, they might have talked with other people about the test and if we then tested on those people they would be more biased when conducting the test. This could be avoided if we told the participants that they should not tell others about the experiment. Furthermore, have them all gathered and then testing them one at a time, while keeping the people that had tested away from the people that were about to be tested.
\\
It was concluded that the validity of the tests and results is lacking and it could be improved by implementing some of the things mentioned above. As for reliability, we tested on 30 people which would increase the reliability of the test but other than that we can not say for certain that the reliability is increased.
\par

\section{Conclusion on Evaluation}
One of the success criteria, namely "the design should be intuitive to the user", is covered in the elicitation mental model test, since the intuitiveness of the physical and visual interface was being tested. Since it was the initial design that was being tested and has later been changed to fit the affordance scheme better, it is assumed that the affordance scheme fits. However, the changed design has not been tested the same way later on, but instead with a mid-fi test. During the mid-fi test the participant were also asked to explain what each button did in relation to the visual interface, as they were asked during the mental model elicitation test. However, the only difference is that the participants interacted with the design before explaining.
\\
To answer the second success criterion "The user should be more encouraged to create music or explore electronic music after using the prototype" each participant were asked after evaluating if they could ever see themselves trying the program again. As the gathered results shows (see \ref{yes_no_table}), more participants said yes to trying it again than participants who said no. This would indicate that people are encouraged to create or explore electronic music. However, since the null hypothesis saying that less participants are more encouraged were accepted, it might not be safe to say that the criterion was fulfilled.
\\
Our last success criterion "Producing a real-life synthesizer should be understood by using common waveform synthesizer techniques" was answered by testing the user on music theory and seeing if they understood this after using the prototype. We found out that some aspects needed to be further worked on but some were understood. The notes themselves and the waveforms were understood but understanding the chords was hard for the users. So it can be said that the criterion was partly fulfilled but if we wanted to fulfil it completely, more tests and changes to the prototype would be needed.
