%!TEX root = ../master.tex
\chapter{User Needs in Context}\label{ch:userneeds}
In order to get an insight into the needs of the target group, it is important that a user study is conducted to find relevant information that can help narrow down the problem. For the user study, a semi-structured interview script was constructed based on the background research and from the expert interview. 

The topics explored in the interview were based on a presentation by Kaufman \citep{tedx_talks_first_2013} and from the expert interview with Jonas Kappel. 

\section{Procedure and methods}
The semi-structured interview was initially created as a set of topics, which it should cover during the interview. Those topics were then distributed under sections which share the same field of interest. As an example, a section of the flowchart in Figure \ref{fig:flow_2} called focus-related, contains all topics related to focus barriers, discipline etc. The other sections are Demographic(see Figure \ref{fig:flow_1}), Skill-learning related (see Figure \ref{fig:flow_3}) and Music experience and knowledge (see Figure \ref{fig:flow_4}).

The semi-structured interviews follow the flowchart topics. First, there is demographic data. Then, the interview covers topics that are related to skill acquisition. This section is heavily based on a presentation by Kaufman \citep{tedx_talks_first_2013}, which covers the time spent to acquire a skill. This includes the user's definition of mastering the skill, e.g. how long does it take to learn something and does that mean they have mastered the skill?
The next section focuses on strategies of how to stay focused, which barriers exist and other focus-related topics. Lastly, the semi-interview covers the music experience and more specifically what chiptune music and synthesizers are.



\begin{figure} [!ht]
\centering
\begin{minipage}{0.45\textwidth}
\centering
\includegraphics[scale=0.4]{flow_1.png}
\exedout % first figure itself
\caption{Interview flowchart: Demographic}
\label{fig:flow_1}
\end{minipage}\hfill
\begin{minipage}{0.45\textwidth}
\centering
\includegraphics[scale=0.4]{flow_3.png}
\exedout % second figure itself
\caption{Interview flowchart: Focus related}
\label{fig:flow_3}
\end{minipage}
\end{figure}

\begin{figure} [!ht]
\centering
\begin{minipage}{0.45\textwidth}
\centering
\includegraphics[scale=0.4]{flow_2.png}
\exedout % first figure itself
\caption{Interview flowchart: Skill-learning related}
\label{fig:flow_2}
\end{minipage}\hfill
\begin{minipage}{0.45\textwidth}
\centering
\includegraphics[scale=0.4]{flow_4.png}
\exedout % second figure itself
\caption{Interview flowchart: Music experience and knowledge}
\label{fig:flow_4}
\end{minipage}
\end{figure}


\par
The participants for the semi-structured interviews were from Aalborg library and Create(Rendsburggade 14). They were asked if they were interested in having a small interview about music and learning. The interviewer also specified that the interview should not take longer than 10 to 15 minutes.
\par
There were two roles for each interview: the interviewer and the notary.
\\ The interviewer was leading the conversation, while the notary was responsible for taking notes and starting the recording once the consent form has been signed.
\par
The interview started with the participant signing the consent form. From that time on, the interview would be recorded on a mobile phone. For validation purposes, we decided to record the audio and take notes. The chosen method for analysis did not rely on audiovisual data, but we wanted to make sure we have the precise answers of the participants, so that the results could be triangulated if some parts were not covered in the notes.
\\
The interview then proceeded according to the flowchart in Figures \ref{fig:flow_1}, \ref{fig:flow_3}, \ref{fig:flow_2} and \ref{fig:flow_4}.

\section{Participants}
The 18 participants were found at Aalborg University campus located at Rendsburggade 14 and Aalborg Library. Ten students were interviewed at Aalborg University. All of those participants were students and eight out of ten were Medialogy students from different semesters. Eight other participants were found at Aalborg Library and most of them were students as well. The age range for the participants spans from 20 to 28 years. 

\section{Analysis}
The gathered sample data was transcribed and categorised into the respective assumptions and analysed. For each of the assumptions, there is a small discussion on how it was used and if there were any complications with it. The results are shown in Appendix A2.

\subsection{Assumptions}
\subsection*{More than 35\% of the people who have never tried creating music do not know how to start}
Out of 18 participants, only five fit the premise, since most of the participants had already tried creating music. It is therefore hard to conclude on this assumption, since the number of participants in the sample is not large enough. However, out of the five participants, two of them have never tried creating music and do not know how to start. This makes 40\%. The result is not reliable due to the small size of the sample.


\subsection*{Learning can be hard and time consuming when starting up and might not lead to what one expects to have learned at the end (general)}
Since this assumption covers different areas, e.g. difficulty, time consumption and expectations, it was analysed in three parts covering these areas. It was impossible to conclude on as a whole because of the feedback gotten from the interviews. The three areas are strategy, time \& skill perception and focus.
 
\subsubsection*{Strategy}
For strategy, there is a broad selection of answers given to what is used to acquire a new skill. Some of the different sources of learning were reading articles/books, using the internet, learning from another user with more knowledge about the subject and attending classes/sessions. Seven participants answered that they read articles and books to understand the skill they wanted to learn. Ten use the internet to look up more about the skill and watch videos or sites that were related to their skill. Five participants asked other users that were more knowledgeable about the skill or instructors for help and one attended classes/sessions for learning the skill. Furthermore, it was also shown that 13 of the participants are more keen on doing self study on the skill and six participants were working in groups to learn their skill.

Half of the participants use internet or read about the skill they want to acquire, and half learn it better by self studying. This might indicate that, when acquiring a skill, the best source for learning more about a skill is the internet, as well as reading books and articles to self study, as the skills they are trying to learn are something that is only acquired individually, and not cooperatively.

\subsubsection*{Time \& Skill perception}
Regarding time and mastering versus learning, most of the participants said that learning a skill and its basics does not take a long time. A few said a couple of weeks, some said less. Mastering, however, seems to split participants' answers, with someone quoting Gladwell’s 10,000 hours \citep{gladwell_outliers_2008}, while some mentioned that one can never truly master something. Furthermore, one participant mentioned that mastering something means that one can combine it with other things. One participant mentioned that it specifically takes four hours each three days, meaning it takes 12 hours to learn something. This comes close to Kaufman’s First 20 Hours \citep{tedx_talks_first_2013}.
In general, the participants perceive learning as having the basics, the constraints and the ability learned.

\subsubsection*{Focus}
 Seemingly, there is quite a difference of opinion with regards to music - five participantss mentioned it as a good tool to cancel out noise or procrastination, but another five people mentioned any loud noise and music can set them off. The biggest factor for people is the interest or motivation - it helps them to stay focused. Some of the strategies of how to stay focused are to split the learning into building blocks, starting small and take on more advanced topics as one progresses. Another factor was some kind of checklist or deadlines - this usually goes tightly with a sense of having a goal.
Barriers, apart from the noise and music discussed before, were mostly social media, e.g. Facebook, and interference from the outside, such as family or some kind of environment distraction. A couple of participants also mentioned that having too many things to learn or too much of a challenge can discourage one from doing it. 

It is hard to conclude whether it is confirmed or rejected, since the assumption is too vaguely formulated.


\subsection*{The learned skill cannot be used out of context to another subject}
Many of the participants did not fit the premise, since the question was not asked clearly enough, which resulted in the participants not giving a response that would answer the assumption. A participant mentioned that a skill can be applied in other contexts, especially in music in order to differentiate music instruments for example.

\subsection*{More than 50\% of people do not know what a synthesizer is}
The participants were asked if they know what a synthesizer is. 55.6\% of the participants do not know what it is. The participants who said yes, were asked to explain, in their words, what a synthesizer is. This was done to control whether or not the answer was valid. This confirms the assumption. 

\subsection*{More than 50\% of people do not know what chiptune is}
This assumption is rejected, since 35.2\% of the participants answered that they do not know what chiptune is.

\subsection*{More than 50\% of people think that music is primarily associated with instruments}
The participants were asked if they create music, where 11 out of 18 participants create music. Afterwards they were asked how they create music. Most of them, in fact 81.8\%, creates music with instruments. Only two participants used sampling to create music. From this information, we can interpret that the majority associates music primarily with instruments. Therefore this assumption is confirmed.

\section{Discussion}
The coded script, was made by two group members and therefore every member might not have known the context for how these questions and assumptions were made. This meant that the interpretation of the analysis could have been difficult for those who did not make the actual script and assumptions. Furthermore, for the first half of the user study the semi-structured interview was not conducted by the same members who made the script. This meant that the questions might not been asked as planned in the procedure. 

Since most of the participants study Medialogy, it makes the sample very specific and difficult to tell if some of the results are applicable to the entire target group. The question regarding chiptune music was created in such a manner that it was a binomial question, where the participants could only answer a ‘yes’ or ‘no’. The question about chiptune was asked in a manner that made it unclear whether the question was about 8-bit in general or 8-bit music. This made it unclear whether they answered the question in regard to knowing the genre or something else entirely. 

The second assumption was too vaguely phrased because it needed to meet the following conditions: starting to learn something can be hard, time consumption and people perceiving a skill by the entire thing rather than small goals in that skill. Therefore it was difficult to conduct the analysis for this assumption. The assumption was split into three smaller areas, but it was done during the analysis and was not part of the original plan. The areas were not entirely mapped to the questions. 

\section{Conclusion}
The findings from the user study cover strategy, focus, barriers and how much time is spent on learning. A major problem of the user study was that the sample of the target group simply was not big enough and therefore it is hard to draw a general conclusion about the target group. However, a tendency to learn a skill by reading, watching videos or attending classes means that most learn by seeing what others do and learn from them, which means that they learn by imitating. The most important prerequisite of learning was to have an interest and motivation. Interference from outside seemed to be the biggest obstacle. This includes family, social media and disturbances from loud noises. This indicates that most of the participants learn by being by themselves. Furthermore, it was found that the majority of the participants did not know what a synthesizer is, but know what chiptune is. This partially fits the last assumption that more than 50\% of the people think that music is primarily related to instruments. Perhaps if more people related music to sampling instead of instruments, they would know what a synthesizer is. The findings from the user study are utilised to finalise the problem formulation.
