%!TEX root = ../master.tex
\chapter{Program Description}\label{ch:prgdesc}

\section{Cross-platform Communication}\label{s:crossplatform}
The prototype incorporates three distinct platforms for three distinct purposes:
\begin{itemize}
\item Arduino to read and write to electric circuits,
\item Unity3D to provide visual feedback to reflect the user's choices, and
\item Processing to provide auditory feedback.
\end{itemize}

This subsection goes through the communication that happens between these platforms, without which the platform would never work. This section goes through the communication from its start to finish, but it does not describe the aforementioned three purposes, only the communication between them.

\subsection{Arduino and writing to Serial}
The Arduino is connected through a USB cable to a host computer and this connection is mode of communication between Arduino and host. Arduino calls the \lstinline!println()! function to write a message to the Serial, and this method is used in the five different Serial-writing functions:\\


\lstinline!sendNote()! Sends the information about a note as it is being pressed.\\
\lstinline!sendPressedArray()!
Sends the information about when the note was pressed as it is released.\\
\lstinline!sendBeat()! Sends the information about the beat being pressed, including when.\\
\lstinline!sendNavigation()! Sends the information whether the user clicked "up" or "down" in channels.\\
\lstinline!sendUndo()! Sends the information when the user presses undo button.\\
Not all is explained in this subsection, but certain things need a little explanation for clarification.
\par 

\lstinline!sendNote()!This function goes through all note buttons, waveform value, channel selected, and the chord buttons state. As can be seen below, it appends all of these messages together and sends that message to Serial (if they are not a beat):
\begin{lstlisting}
        message = String(buttonState[x] + x) + "," + String(y) + "," + String(channel) + "," + String(chord);
        playedPos[counter] = 1;
        if (x+1 == numberNotes) {
          message = "n"; 
          sendBeat();
        }
        else {
          Serial.println(message);
        }
\end{lstlisting}

This function also fills in the \lstinline!playedPos! array at position counter (the current position from zero to eight).
\par 

\lstinline!sendPressedArray()!
This function checks whether or not a button has been pressed by comparing it to its former state:
\begin{lstlisting}
    if (buttonState[t] != lastNoteState[t]) {\ldots}
\end{lstlisting}

It then writes a new message consisting of the last message registered in \lstinline!sendNote()! and appending each value of the \lstinline!playedPos! list separated by commas:
\begin{lstlisting}
 String playerPosMsg;
          for (int i = 0; i < 8; i++) {
            if (i < 7)
              playerPosMsg = playerPosMsg + playedPos[i] + ",";
            else
              playerPosMsg = playerPosMsg + playedPos[i];
          }
          
          //Print the final message to serial
          playerPosMsg = message + "," + playerPosMsg;
          if (message !=  "")
            Serial.println(playerPosMsg);
\end{lstlisting}       

\subsection{Unity reading from serial and sending with UDP}
Unity reads from the USB port which the Arduino is connected to with an asynchronous coroutine called \lstinline!AsynchronousReadFromArduino()!. It also makes use of \lstinline!System.IO.Ports! to read from serial port. 
\par 

The \lstinline!AsynchronousReadFromArduino! is based on an article by Alan Zucconi, slightly modified to fit the needs of this prototype. The most important parts of the function are the following:
\\
\begin{lstlisting}[language=csh]
do {
			try {
				dataString = stream.ReadLine ();
			} catch (TimeoutException) {
				dataString = null;
			}

			if (dataString != null) {
				started = true;
				incMessage = dataString;
				Debug.Log("Message from Arduino: " + incMessage);
				callback (dataString);
				SplitMessage();
				spawning = true;
				yield return null;
			} else
				spawning = false;
			yield return new WaitForSeconds (0.05f);

			nowTime = DateTime.Now;
			diff = nowTime - initialTime;
		} while (diff.Milliseconds < timeout);
\end{lstlisting}
In which Unity is reading from the \lstinline!SerialPort! stream, calling \lstinline!SplitMessage! to format the message and controlling the spawning state. It also has a timeout of 0.5f seconds (500 milliseconds) in order to reduce the incoming load.
\par 

\lstinline!SplitMessage! is based on an earlier created function in Processing, which has the purpose of reading all the important parts of the incoming message, splitting them up and converting them to values usable to other scripts in Unity. The following line makes use of string \lstinline!Split()! function that takes a string and separates by a unique character. In this case it is a comma (","):
\\
\begin{lstlisting}
string[] messageParts = incMessage.Split (new string[] {","}, StringSplitOptions.None);
\end{lstlisting}

\lstinline!SplitMessage! also recognises if the message is still coming in or the "final" message was sent, by analysing the length of the message. Furthermore, it looks at the first character in the string and does a number of different things depending on input:
\begin{itemize}
\item If the message starts with a "n", this means it is a beat and it should call functions related to such.
\item If the message starts with a "u", this means it is undo and it should call Undo functions in certain object(s).
\item If none of the above is determined, it is a still incoming note being pressed, and it starts spawning a planet related to such. 
\end{itemize}

Since the serial port is being listened to by Unity, the other program, Processing, cannot listen as well. Therefore, UDP was utilised to send a message to Processing as soon as Unity gets a message from Arduino. This happens in the UDPSend script which was inspired by la1n's post on Unity3D forums \citep{unity_community}.
The most important parts of this script happen in \lstinline!init()! and \lstinline!sendString()!. In \lstinline!init()! the script makes a new UDP server and makes ready to send to 127.0.0.1 through port 12001. In \lstinline!sendString()! it encodes the arduino message into a byte array and sends it to the client. These two functions are illustrated below:
\begin{lstlisting}[language=csh]
public void init() {
	print ("UDPSend.init()");

	IP = "127.0.0.1";
	port = 12001;

	remoteEndPoint = new IPEndPoint (IPAddress.Parse (IP), port);
	client = new UdpClient ();

	print ("Sending to: " + IP + " : " + port);
	print ("Testing: nc -lu " + IP + " : " + port);
}
\end{lstlisting}
\begin{lstlisting}[language=csh]
private void sendString() {
		try {
			byte[] data = Encoding.UTF8.GetBytes(gm.tempMessage);
			client.Send(data, data.Length, remoteEndPoint);
		} catch (Exception err) {
			print (err.ToString ());
		}
}
\end{lstlisting}

\subsection{Processing reading from Unity via UDP}
As mentioned above, Unity communicates with Processing through UDP and acts as a server sending the information it gets from Arduino. This means that Processing acts as a client to this connection and receives the packets sent from Unity. This happens through the help of the hypermedia.net library, the use of which can be seen below:
\begin{lstlisting}[language=Java]
  udp = new UDP(this, port, hostIP);
  udp.log(true);
  udp.listen(true);
\end{lstlisting}
As can be seen, Processing is told to listen to a certain port (12001) and Internet Protocol (127.0.0.1).
There are standard methods that need to be implemented, most importantly receive(byte[] data) which can be overwritten to handle more parameters. The implemented version can be seen below:
\begin{lstlisting}[language=Java]
void receive(byte[] data, String ip, int port) {
  String message = new String(data);
  val = message;
  if (message != "") {
    pressed = true;
  }
  println("Message is: " + val);
}
\end{lstlisting}
In this code the data received is casted to a \lstinline!String!, saved to a global variable and checked if it is empty. The global variable is then used in several places, and most importantly: in \lstinline!splitMessage! described below.

As mentioned before, the \lstinline!splitMessage! function in Unity has a lot of similarities with the one used in Processing. The differences are:
\begin{itemize}
\item When a note is being pressed, the information received by processing is of the form "note, waveform, channel, chord" and is stored in a temporary Input object, to later on be utilized during sound output.
\begin{lstlisting}[language=Java]
tempInput = new Input(-1, note, waveform, chord);
\end{lstlisting}
\item When a note is being released, the message sent to Processing is of the form "note, waveform, channel, chord, the duration of the sound". In this case, the message is stored in a constant Input object with the help of a constructor, that puts the duration of a sound in a separate boolean array. The array consists of eight booleans, each of them representing a beat. If the value is true, it means that the sound is played at that time. If it is false, then the sound is not played. 
\item When a message starting with an "n" is received, the program uses a special Input object constructor, that is designed specifically for noise input.
\begin{lstlisting}[language=Java]
tempInput = new Input(id, 3, 0, frameToBoolean);
\end{lstlisting}
\item The constructors of the Input object are stored in a separate Note class, together with the necessary methods, such as getters and setters. 
\item All of the constant \lstinline!Input! objects are stored in an \lstinline!ArrayList!, in order to have a quick access to them when necessary. 
\end{itemize}
Important parts of the Processing version of \lstinline!splitMessage! is illustrated below:
\begin{lstlisting}[language=Java]
void splitMessage(String message) {
  if (message != "") {
    parts = split(message,','); 
    frameToBoolean = new boolean[16];
\end{lstlisting}
(...)
\begin{lstlisting}[language=Java]
note = Integer.parseInt(parts[0].trim()); 
    waveform = Integer.parseInt(parts[1].trim());
    int channel = Integer.parseInt(parts[2].trim());
    int chord = Integer.parseInt(parts[3].trim());
    tempInput = new Input(-1, note, waveform, chord);
    if (parts.length != 4){
      makeFrames(4, parts);
      tempInput = new Input(id, note, waveform, channel, chord, frameToBoolean, frameToBoolean);
      ++id;
\end{lstlisting}

\section{Physical-to-digital mapping with Arduino}
The Arduino is reading from the circuit, as described in \ref{subsec:physArt}, either through analog ports or digital ports. This subsection briefly explains through a few examples how the code running on the Arduino reads from and writes to different parts of the circuit.
The code is written in and with Arduino Software IDE which is based on programming languages, such as Processing.

The program utilises both digital read/write, as well as analog read/write. The former is shown here through a function which reads a certain group of buttons "Chords" used for adding more notes to a base note:
\begin{lstlisting}
  int button1State = digitalRead(dyadButton);
  int button2State = digitalRead(triadButton);
  int led1State = digitalRead(dyaLed);
  int led2State = digitalRead(triLed);
\end{lstlisting}

The program uses analog reading as well, to read more states than just 0/1, Off/On. In this circuit, a potentiometer was used which was separated into four ranges as shown in the code below:
\begin{lstlisting}
void waveformButton() {
    val = analogRead(potPin);

  if (val <= 256){
    digitalWrite(led[0], HIGH);
    digitalWrite(led[1], LOW);
    digitalWrite(led[2], LOW);
    digitalWrite(led[3], LOW);
  }
  else if (val > 256 && val <= 512){
    digitalWrite(led[0], LOW);
    digitalWrite(led[1], HIGH);
    digitalWrite(led[2], LOW);
    digitalWrite(led[3], LOW);
  }
  else if (val > 512 && val <= 768){
    digitalWrite(led[0], LOW);
    digitalWrite(led[1], LOW);
    digitalWrite(led[2], HIGH);
    digitalWrite(led[3], LOW);
  }
  else {
    digitalWrite(led[0], LOW);
    digitalWrite(led[1], LOW);
    digitalWrite(led[2], LOW);
    digitalWrite(led[3], HIGH);
  }
}
\end{lstlisting}
As seen, the value read from the analog is then written to LEDs indicating which one is active. These LEDs are then read from at a later point:
\begin{lstlisting}
int ledState = digitalRead(led[y]);
\end{lstlisting}
In this line, "y" is indicating the current led being read from the group of LEDs.

Since the whole prototype depends on timing, it was very important to impose a structure in which messages would be sent from the Arduino to the rest of the programs. This was done by delaying for 500 milliseconds. However, the other programs, Processing in particular, needed to know when certain sounds so should be played. In order for that to be enforced, the Arduino utilised a counter to go from zero to seven, effectively making it possible to add a new sound every half second for four seconds: eight positions. This counter variable was controlled in the main loop of the program:
\begin{lstlisting}
  if (counter == 7) {
    counter = -1;
  }
  if (startCount) {
    counter++;
    delay(500);
  }
\end{lstlisting}
Whenever one holds down a button in the circuit, the program notes where in the counter it is and overwrite the position in the \lstinline!playedPos! list, effectively outputting a number in a eight-position comma separated message: 0,0,0,0,0,0,0,0. This happens only when a user lets go of a button, not before.

\section{Visual feedback with Unity}
For the prototype, we decided that we needed something visual for the user to interact with. All the visual feedback was made in Unity using different scripts and different functions. In this subchapter, we go through some of the more important ones. 

\begin{figure} [!ht]
	\centering
	\includegraphics[scale=0.3]{Snappoints.jpg}
	\caption{Snappoints in Unity scene}
	\label{fig:snappoints}
\end{figure}

Firstly, the function that spawns the planets called add to \lstinline!addToArrayList! and in here the object is instantiated, but in order to do that it uses a system we call the snap point, shown in Figure \ref{fig:snappoints}. This looks at the current scene where there are several invisible objects and it finds the one closest to the top. It then gets the position from that invisible object and places the planet at that position. To get the position of the planets, we stored it as an object using the planet class which stores two integer values which represent the position of the snap point the planet is attached to.
\begin{lstlisting}[language=csh]
class Planet
{
    private int ring;
    private int pos;
	private int id;

    public Planet(int tempRing, int tempPos)
    {
        ring = tempRing;
        pos = tempPos;
    }

    public int GetRing()
    {
        return ring;
    }
    public int GetPos()
    {
        return pos;
    }
	public int getID() {
		return id;
	}
	public void setID(int newID) {
		id = newID;
	}		
}
\end{lstlisting}
It then checks if there is a planet on this position. The way it is done is that the name of the object is changed to what snap point it is currently at. If there is something there already, then it replaces it with the new input and save the old one for the undo button.
The undo simply removes the last added planet by finding the last used snap point and removes the planet there. If any planets were overridden, then they should be put back into the scene.
\begin{lstlisting}[language=csh]
	GameObject deletePlanet = GameObject.Find ("Planet " + planetList[i].GetPos () + planetList[i].GetRing ());
	GameObject deleteBeat = GameObject.Find ("Beat " + planetList[i].GetPos());
	Destroy (deletePlanet);
	Destroy (deleteBeat);
\end{lstlisting}

Another important part of the program is the \lstinline!ForwardDetection! which checks the planet in front of every planet if they have the same attributes. It does this by looking at tags that are attached to several children of the planet. If the attributes of the one in front of this are the same, then delete only the visuals of the planet.
\begin{lstlisting}[language=csh]
	if (tempMoon.tag == previousMoon.tag && tempWave.tag == previousWave.tag && previousPlanet.name == ("Planet " + previousPos + newRing))
		{
			Renderer ren = tempMoon.GetComponent<Renderer>();
			ren.enabled = false;
			SpriteRenderer sren = tempMoon.GetComponent<SpriteRenderer>();
			sren.enabled = false;
		}
\end{lstlisting}

\section{Generating sound with Processing}
It was decided to play the sound output through Processing, by utilizing Minim audio library \citep{thelen_adsr_2013}. This opens a lot of possibilities when it comes to manipulating sound. In particular, it allows to output every sound through an ADSR (Attack, Decay, Sustain, Release) envelope \citep{_minim_2016}. The program utilizes 14 ADSR objects and stores them in one array, since in total there could be 14 sounds played at once. An example of outputting a sound through an ADSR envelope is given below:
\begin{lstlisting}
tThirdOsc.patch(adsr[adsr.length-3]);
adsr[adsr.length-3].patch(aOut);
adsr[adsr.length-3].noteOn();
\end{lstlisting}

When designing how the sound should be outputted, there were two main tasks that needed to be fulfilled. The former was to output the sound while the user was pressing the button in order to give immediate feedback. The latter was to store the sound and play it continuously over an eight beats per four seconds period. Since the two tasks had to be executed at the same time, it was decided to use multithreading. 

The first thread is called \lstinline!playIncoming!  and is designed to output the sound in order to give immediate feedback to the user. It starts when an input of the form \lstinline!note, waveform, channel, chord! is received. First, it checks whether the input is noise. If so, then a built-in constructor for white noise is being called:
\begin{lstlisting}
adsr[7] = new ADSR(1.5, 0.05, 0.15, 0.1, 0.05);
\end{lstlisting}

Otherwise, a new instance of an \lstinline!Oscil! object is being created:
\begin{lstlisting}
adsr[adsr.length-1] = new ADSR(0.6, 0.04, 0.09, 0.1, 0.1);
\end{lstlisting}

If a major third or seventh chord have been activated, one or two new \lstinline!Oscil! objects would be created with corresponding frequencies and outputted through an ADSR envelope. After the message of the form \lstinline!note, waveform, channel, chord! stops being received, the corresponding sounds stops being played by the program. An example of this can be seen below:
\begin{lstlisting}
adsr[adsr.length-1].noteOff();
adsr[adsr.length-1].unpatchAfterRelease(aOut);
\end{lstlisting}

\begin{figure} [!ht]
	\centering
	\includegraphics[scale=0.4]{bla.jpg}
	\caption{2D array of Input objects}
	\label{fig:2Darray}
\end{figure}

After the note button is released, and the corresponding Input object is formed, it is put into a 2D array of \lstinline!Input! objects, as seen in Figure \ref{fig:2Darray}. It is of size 4x16, with the "width" representing the number of channels available to the user. The last channel is reserved specifically for noise input. Each unit of the "height" represents half a beat. In order to put an Input object into the 2D array, a method called \lstinline!createSoundArray(ArrayList<Input> inputs)! is being utilized. It puts the Input object in the corresponding channel as many times, as there are true valued booleans in its boolean array. 

When the needed sound array is being formed, it can be played out by the thread called \lstinline!playSoundArray()!. It constantly goes through the "height" of the 2D array and starts outputting a sound when a new \lstinline!Input! object is encountered. It also marks a moment when an Input object stops being present, and thus stops playing the corresponding sound. 

When a new constant Input object is being created and it needs to override the existing \lstinline!Input! object, then a method called \lstinline!overrideInputObjects()! is being utilized. It assesses the boolean array of the old Input object, and turns corresponding parts of it to false, thus allowing for the new Input object to be played instead:
\begin{lstlisting}
inputObjects.get(i).setTimeToFalse(inputObjects.get(i).getTime(j));
\end{lstlisting}

