%!TEX root = ../master.tex
\chapter{Feature Development}\label{ch:feadev}
The following chapter covers the process of designing and developing the prototype. Firstly, the methods used for generating the design ideas are described, then the ideas themselves are presented together with the chosen design. This design is then tested through elicitation mental model and low fidelity test (lo-fi), so that concept, affordance and basic interaction were evaluated upon with the users. The implementation of the final design is then described in a feature-by-feature order. Finally, the minimum implementation requirements are listed.

\section{Design Method}
In order to design a system that would help answering the final problem formulation, a few design methods were adopted to make sure to cover the success criteria and other requirements for the users. The design workshop started with a brainstorm session where every member sketched or briefly described as many ideas they could come up with. The time limit for the first round of brainstorming was set to 5 minutes and was increased by one minute if there were members who needed more time to write down their ideas. Afterwards, the design ideas were presented and grouped into different sections depending on similarity in concept of those ideas. A second brainstorm session was conducted to refine specific ideas and narrow down the initial concepts. The time limitation was the same as in the first round of brainstorm session.
\par

After the brainstorming, several designs were chosen based on individual votes. Each member had three votes and they could choose based on their own preference, yet still keeping the final problem formulation in mind. Afterwards, a round robin was conducted based on the ideas. The ideas were elaborated upon by every member of the design workshop. This elaboration was anything from slight design change to adding new features.
\par

The design, which we decided to work with, was chosen depending on its relevance to the problem formulation and the aim of the project.

\section{Design Iterations} 
Throughout the design phase, the synthesizer design idea evolved in both form and interaction. The initial design idea from the preliminary workshop was modified during a prolonged week of iterations, which resulted in a new design. The following design ideas enabled us to come up with the aforementioned design. 
\par

One of the design iterations that we originally set off to develop was a small game, where a stickman controlled by a player needs to run through a level which is created real-time by another user who uses the synthesizer to create the music waves. The goal for the player would be to reach the end of the track, while the goal for the creator of the wave would be to stop the stickman from reaching the end. We also considered switching the roles of player/creator around, so that both users can enjoy both roles. 
\par

After further research into concept of learning through gamification \citep{zichermann_gamification_2011}, we found that the design idea focused more on game aspects than on the aspects of learning how to create chiptune music. In other words, the idea seemed to be forced to fit the game aspect and put the learning aspect into the background. This, however, would not answer the problem formulation and therefore we decided not to limit the users by creating a level design of a game. Therefore, with the next iteration of the idea we decided to focus on a tutorial function and learning instead.
\par

The idea was that the tutorial would guide the user through some basic wave manipulation. The system would show a dotted line for the "goal" - how the wave should look like, and also a normal line which would be the user's input. This wave would change real time depending on the interaction between the user and the synthesizer. The system would provide hints on how to achieve the final look of the wave. After the user creates a wave that is similar or identical to the "goal" wave, the system would proceed to the next part of the tutorial. The tutorial would be an integral part of the learning aspect of the design idea. However, in order to not limit the users in creating music, we also decided to include a "freeplay". This we later also refer to as a "sandbox". This would mean, that the user could play around with the music creation without following any goal other than to create music.
\par

This was a stepping stone for creating a revised sandbox idea, in which we focus even more on creating music freely, without a tutorial. This final design is described in the following chapter. 

\section{Chosen Design}
The design as seen in Figure \ref{fig:solarsys} shows a solar system. The planets are the different notes, and the waveforms are shown as tails to the planets. The rings around the planets indicate if it is either 3rd or 7th chord. The inner circle  holds the beats, which are shown as meteorites with noise signals. The different rings represent the channels, that can each hold 8 planets. The planets are moving clockwise with a start position at the top (north direction) where the planets trigger their sound signals. After a note is added to the system, it automatically snaps into the nearest position, therefore sometimes it overwrites already added planets. Those positions are later referred as "snap points". The system was designed to be build with an Arduino, breadboards and variety of sensors.
\par 

\begin{figure}
	\centering
	\includegraphics[scale=0.4]{solarsys.png}
	\caption{A simple synthesizer designed to represent a solar system.}
	\label{fig:solarsys}
\end{figure} 
\par 

\section{Elicitation Mental Model}
Since the prototype should be intuitive and easy-to-use for the user, elicitation mental model can be utilised to test whether or not the interface design suffice in those categories. The following subchapters, procedure, results and analysis are described. Eventually, the conclusion is drawn, which then refers back to the development of the prototype. 

\subsection{Procedure}
For the elicitation mental model testing, a cardboard prototype of the physical interface (see Figure \ref{fig:solarsys}) and a paper prototype of the visual interface were made (see Figure \ref{fig:cardboard}). Each section has a label with a title and the corresponding buttons are grouped together to enhance the proximity. 

\begin{figure}
	\centering
	\includegraphics[scale=0.2]{cardboard.png}
	\caption{Cardboard representation of the physical interface.}
	\label{fig:cardboard}
\end{figure} 

An affordance scheme of the system was made in order to specify what each of the buttons do and how they should be perceived (see Appendix A3). The scheme was used in relation to the gathered results.
Before conducting the test, the participants were briefly introduced to the project and the procedure of the test. After the introduction, the participants were asked to perform two tasks; explain what function they think the buttons on the physical interface has and if they can associate what the visual interface represents in relation to the buttons. A notary would write down what the participant explained. Seven participants in total were tested and each of the testing sessions were recorded, so that the results could be compared to the notes and therefore increase the validity of the results. The setup of the test can be seen on Figure \ref{fig:setup}.

\begin{figure}
	\centering
	\includegraphics[scale=0.3]{setup.png}
	\caption{Setup of the elicitation mental model testing. Participant to the right and interviewer to the left.}
	\label{fig:setup}
\end{figure} 

\subsection{Results}
From the test, a lot of data was gathered. For the note-buttons, containing A, C, D and G, the majority thought that pressing the buttons would create a note. Few of the participants thought that pressing the button would select it. This was probably due to the fact that the tested visual interface was already in a "filled-in" stage, so that the participant would not necessarily know if one can add sounds to the interface or only modify them. Only one participant thought that holding down the buttons would create longer notes, which was the desired answer.
\par 

The majority of the participants did not understand the buttons labelled harmonics. One thought it would change the length of the waveforms and some thought it changes the notes from fundamental frequency to its nth harmonic.
For the navigation, up and down, about half of the users thought it moves the arrow between the different rings and the other half thought it moves the notes between the different rings. One thought the buttons would select the different notes.
\par 

When the participants had to explain the knob for the waveforms, some thought it had something to do with the wavelength and the sound. Some also thought they would change the mode or the look of the notes tail. 
There were also different responses to the noise, also called beat. Some mentioned that it produces a random sound or noise that can be modified. Some thought the button had something to do with the noise in the middle of the visual interface and some were not sure what noise was.
For the undo button, all the participants thought it would delete the latest action.

\subsection{Analysis}

In this subchapter, the responses from the test are related to the affordance scheme (see appendix A3). In the affordance scheme, we look into different states that the system has, the perceived affordance, the feedforward and feedback. The different states represent the change in the system, for example a light bulb can be on and off.  "Affordance refers to the properties that things have (or are perceived to have) and how these relate to how the things could be used." \citep[p. 90]{benyon_designing_2013}. That is, e.g. a button that can be pressed. Feedforward tells the user what they can expect by interacting with the different parts of the system, e.g. what happens when a button is pressed. Feedback, on the other hand, informs the user what is the response of the system and subsequently lead the user into follow-up actions \citep{benyon_designing_2013}
\par 

Since the majority of participants answered that they thought that pressing the notes would create it, the perceived affordance is correct, but since only one participants thought that holding down the button, which is the real design, would create it, the perceived affordance might need be to be changed to indicate this. Furthermore the participants did not know that by holding the note button down for a prolonged time would create a longer tail. Therefore this was never known to the participant.
\par 
The majority of the participants did not know what the chords would change, but this is most likely due to wrong labelling of the buttons. We then later decided to rename the label from harmonics into triadics for better clarification. The participants mostly agreed that the LED would light up when pressed, but many users were confused as to what pressing the buttons would change both visually and musically. Therefore, the perceived affordance did not fit, so the design needs to be modified further.
\par 
Half of the answers from the test showed that the participants understood the navigation as part of moving the arrow up or down between the rings. Also half of the participants answered that it was the notes that would be moved. This could indicate that in order for the rest to also understand it the same way, it would require that the buttons are better explained on the interface. This has something to do with some of the participants thinking that the system is already created and they can only select and modify the notes. The participants did not realise that they would start with an empty galaxy.
\par 
More than half of the participants understood the connection between the visual look of waveforms and the corresponding wave selection knob on the interface. Partcipants also generally knew that waveforms had something to do with wavelength and sound. Therefore the preliminary affordance scheme more or less fits. 
The test showed that half understood what the noise button was for and half did not understand it. There was also a small sample that said they did not know what the button was for. This could indicate that the button should be better at explaining what it does. Either by changing the title for the button, or to change the name and not call it noise. Therefore the preliminary scheme cannot be confirmed.
\par
Undo button was understood correctly by all of the participants. The preliminary affordance scheme fits for this button setting.

\subsection{Conclusion}
It was found from the elicitation mental model test that the design had to be changed in order for the affordance scheme to fit better. Some of these changes are that it should be better indicated that holding down a note-button would create a longer note. Generally participants did not understand what the chords did due to the misleading labelling, therefore that was changed in further iterations of the interface. The visual feedback should also help with explaining that the navigation buttons selects the rings and not the notes. The final change would be the noise button as its label seemed to confuse the participants.

\section{Low Fidelity Test}
Lo-fi test was conducted in order to evaluate upon not only the basic concept of the project, but also some light interaction with the program. 

\subsection{Procedure}
Ten participants were tested for the lo-fi at Create building. The test was conducted in a silent room to ensure that there would not be too many distracting factors. For the test we made a paper prototype and pieces that could be moved around, so that the user could interact with the prototype. We also recorded all the tests with a camera which was used to triangulate the data by comparing the notes and the recordings. We had two roles for this test: an interviewer and a notary. The interviewer was in charge of asking the question and moving around the pieces as the participants interacted with it while the notary was in charge of taking notes on both what they said and what they did.
\par 

Before conducting the test, five different hypotheses were created. The hypotheses were coded with numbers. Questions and observation guidelines were created from these hypotheses and coded with the belonging number for an easier analysis. These assumptions are listed as follows.
\par 
\begin{itemize}
\item The user will understand the correct procedure of creating a note at least 70\% of the time.
\par

\item The user will understand that holding down the note buttons will determine the length of the note at least 70\% of the time.
\par

\item The user will know how to create a beat at least 90\% of the time.
\par

\item The user understands how the visual interface behaves from interacting with the physical interface at least 90\% of the time.
\par

\item The user understands the function of each button at least 80\% of the time.
\par

\end{itemize}
Three different scenarios were set up for the test. The scenarios were increasingly difficult. In the first scenario, the participant was asked to create a beat. In the second scenario, the participant was asked to create any sound. In the final scenario, the participant was asked to undo the previous action and create a specific sound. During the test, the participant was told to speak out loud as the scenario was performed. Whenever a button was pushed or knob was turned, the interviewer would simulate the action with paper prototypes on the visual interface and therefore give the participant feedback. After the three scenarios, a few questions about the physical and visual interfaces were asked.
\par 

\subsection{Results and Analysis}
These are the hypotheses and their respective results after being collected(see Appendix A5).Furthermore, the percentage for the assumptions were an estimation of success based on the elicitation mental model.

\textbf{The user will understand the correct procedure of creating a note at least 70\% of the time}
\\
70\% do not know how to create a note with the right procedure. All, but one participant, figured out how to create a note by selecting a waveform and pressing a note, but without using the chords. However, some of them tried to pick a chord, but did not figure out what it did exactly to the creation of a note. Therefore this hypothesis is rejected.
\par 

\textbf{The user will understand that holding down the note buttons will determine the length of the note at least 70\% of the time}
\\
100\% of the participants did not figure out that holding down the note buttons will determine the length of the note. This hypothesis is rejected.
\par 

\textbf{The user will know how to create a beat at least 90\% of the time}
\\
90\% knows how to create a beat, by pressing the “beat”-button. This hypothesis is confirmed.
\par 

\textbf{The user understands how the visual interface behaves from interacting with the physical interface at least 90\% of the time} 
\\
80\% do not understand how the visual interface behaves from interaction with the physical interface, due to not knowing what the chords did. The hypothesis is therefore rejected.
\par 

\textbf{The user understands the function of each button at least 80\% of the time}
\\
The participants understand everything, but the buttons for 3rd and 7th chords. All of them, but one, did not understand these buttons, probably because it had the misleading label "triadic". Therefore, this hypothesis is rejected.

\subsection{Conclusion}
Most of the hypotheses were rejected due to the participants not understanding what the buttons with chords do to the creation of notes. From this, we decided to rename the chord label from triadic into chords.
\par 

It was also found that the participants did not know that holding down the "note"-buttons would create a longer note. Therefore, we also need to find a way to tell the users to hold down the buttons to create a longer note instead of just pressing them.

\section{Features of the Final Design}
This subchapter explains the implementation of the final design. The work was divided into features, so that more of them could be worked on at the same time. The visual representation, which was developed in Unity, was done in 8-bit style, so that it would fit the theme of the project. The sound generation was done through Processing, the visual representation was made in Unity and the physical system was built using an Arduino and a variety of components.

\subsection{Solar system dynamic}
It was decided that the program should look like a solar system. Therefore, we needed a few of the core mechanics that users associate solar systems with. First, we decided to make the planets rotate around the sun in the center. This was done by setting the sun as a point and then making the planets rotate around that point. 
\par 

All planets make a full circle around the sun in four seconds, no matter the channel. Furthermore, we decided to make the planets rotate around themselves to make the them seem more realistic and interesting to look at.

\subsection{Spawn notes}
Since the system was designed to look as a solar system, the notes are graphically represented as planets. The different notes have different colours of the planets (see Figure \ref{fig:finalSolar}).

\begin{figure} [!ht]
	\centering
	\includegraphics[scale=0.3]{finalSolar.png}
	\caption{Visual representation of the different sounds produced by the system.}
	\label{fig:finalSolar}
\end{figure}

Blue - note D\\
Red - note A\\
Green - note G\\
Grey - note C\\
Purple - note E\\

\par 



When the user presses a note or beat button, a planet is spawned and begins to rotate around the sun. At the same moment, the corresponding sound is played at a rate two beats per second to allow the user to play two notes in a one second interval, if desired. Each note can be played using different waveforms. It is also possible to locate it on one of the three channels and choose to play it with a dyadic or triadic chord. Those properties of a note, as well as the properties of the beat button, are described below.
\par  

\subsubsection{Waveforms}
There are four types of waveforms for a note button: square, quarter pulse, triangle and sawtooth. Each of these can be utilized in chiptune music \citep{driscoll_endless_2009} and can imitate different instruments. To produce those waveforms, the Fourier theorem can be used. According to it, all types of waveforms can be produced by adding a number of sine waves together \citep{steiglitz_digital_1996}. A square wave is produced by adding the odd numbered harmonics together. The resulting sound is quite hollow and can be used to imitate basses \citep[p. 11]{hewitt_music_2008}. The quarter pulse wave is similar to a square wave, but has a different duty cycle. Its sound is a bit similar to that of an oboe \citep{sos_synth_2000}. The triangle wave is also formed by using only odd numbered harmonics in a specific way \citep{pure_data_floss}. It produces a clear sound and can be utilized to simulate the sound of a flute \citep[p. 11]{hewitt_music_2008}. The sawtooth wave utilizes all of the harmonics \citep{pure_data_floss} and can be used for string and brass sounds \citep[p. 11]{hewitt_music_2008}. Visually, the waveforms are shown as "tails" of a planet. This idea was, in part, inspired by the look of the comets.
\par

\subsubsection{Note}
Each note is defined by its frequency, also referred to as the pitch of the sound \citep[p. 3]{hewitt_music_2008}. There is a general standard stating that a note should be of a certain frequency, no matter the instrument it is being played on. For example, note A should always have a frequency of 440 Hz \citep[p. 4]{hewitt_music_2008}. Since there were some limitations during the development of the prototype, it was decided to implement only five of the popular notes \citep{buskirk_most_2015}, namely the fourth octave of C, D, E, G, A. Additionally, it was planned, that the planets would have a letter representing their note shown on them. However when the option of having chords was added, we decided to only keep colour coding as it began to be hard to read.


\par 

In order for the notes to sound like they are played through an instrument and to make them generally more pleasant to the ear, each of them is played through a sound envelope (see Figure \ref{fig:envelope}). It consists of four parts: attack, decay, sustain and release. They are affecting the amplitude over the time a note is being played \citep[p. 12]{hewitt_music_2008}. Each instrument has its own envelope parameters \citep{wiki_audio_adsr_2010}. In the prototype, however, the same parameters are used for each note, no matter the waveform. The main reason for doing so was the decision to have it in later versions of the program.

\begin{figure} [!ht]
	\centering
	\includegraphics[scale=1.0]{envelope.png}
	\caption{Envelope properties}
	\label{fig:envelope}
\end{figure}

\par

\subsubsection{Chords}
When playing two or more pitches simultaneously, one performs a chord. Chords are split into two categories: dyads, which is playing two pitches at the same time, and chords, which is playing three or more pitches at the same time \citep{triads_chords}. A chord also consists of the root pitch. The root is the lowest pitch which will be used to create a chord. To create a full chord one has to find the 3rd and the 5th. These can be found  by moving up the note scale by 4 half steps from the root to get the 3rd, and 7 half steps to get the 5th. A 7th can also be used, in which one has to move up the note scale by 11 half steps from the root position\citep{triads_chords}.

\par
A chord can have different qualities, depending on what one needs. There are in total four different qualities; Major, minor, diminished and augmented. The different qualities use sharp notes which is notes with the \# sign, or flat notes that are shown with $\flat$ \citep{triads_chords}. An example of a triadic chord can be seen in Figure \ref{fig:triadic_chord}.

\begin{figure} [!ht]
	\centering
	\includegraphics[scale=1.0]{triadic_chord.png}
	\caption{C major chord with its corresponding 3rd and 5th from the site\citep{triads_chords}.  
}
	\label{fig:triadic_chord}
\end{figure}

\par
For the triad and dyad features, 3rd and 7th are going to be utilized, as they are the most commonly used in old game consoles\citep{lftkryo_elements_2011}, where the chiptune genre originates from. The reason why 5th is not used in the prototype is because in earlier game consoles for example Famicom, there was not enough processing power to run true chords. Therefore 5th was excluded. Furthermore, the quality of the triads is preset to major in the prototype. 

\par
Visually, a chord is represented as a ring around the planet. This idea was inspired by the look of Saturn. 
\par

\subsubsection{Channel}
In the prototype, there are three channels that are used to store the note in. In earlier game consoles, the sound board consisted of four channels which had a specific waveform attached. Those were triangle, two pulse and white noise \citep{driscoll_endless_2009}. For the prototype, instead of having one specific waveform to each channel, they are instead used as a sequencer, that will run continuously, and each channel can use any kind of waveform. Visually, the channels are represented as the rings around the sun.

\subsubsection{Beat}
Beat is when two sinusoids are close to each other, but are not identical. When the two sinusoids phase in and out to each other, a beat is created \citep[p. 14-15]{steiglitz_digital_1996}. In order to do the same in the prototype, white noise is used. The white noise is changed with the envelope to sound more as a drum kick, while still resembling chiptune music. Furthermore, this was in correlation with how a beat kick sounded in some of the earlier game consoles, for example Famicom. 
\par
Visually, a beat is represented as an asteroid that rotates in the innermost circle of the sun. This is achieved by reserving the fourth channel only for the beats. Unlike a note button, where the duration of the sound depends on how long the user is holding down the button, the beat button will produce several beats when pressed continuously for more than half a second, both visually and audibly.

\subsection{Override and remove planets}
We needed to allow the user to override and delete planets to give them as much freedom when creating music as they want. We allow the user to override the current planet with a new planet, so that they can change the sounds being played constantly. Once a planet is overwritten, the previous planet is made invisible, as we use this data for enabling the user to go a step back. The new planet, which then appears on the top of the invisible one, is shown. 

\par 

The undo button, if pressed, removes the last added planet and makes the planet underneath visible. However, if there was no planet underneath, the planet is only removed. \\
The user can only go one step back at a time, as the system only remembers the planet that was added last. Therefore if a new planet is added, the previously added planet is no longer stored and cannot be undone. It can, however, be overridden. 
\par 

When a planet is overridden the sound will also have to change. Audibly, the sound will be replaced with the new sound that the user created.

\subsection{Physical artefact}\label{subsec:physArt}
Before building the circuit for the physical artefact, a blueprint was made in Autodesk 123D Circuits (see Figure \ref{fig:blueprint}). 123D Circuits has presets of most of the components needed to build the circuit, including an Arduino and breadboard. One also has the possibility to write the fitting code and simulate the circuit to make sure it works properly before actually building it with real components. The blueprint was made with two Arduino Uno's since 123D Circuits did not offer an Arduino Mega 2560, which has the necessary amount of digital pins. Using two Arduinos in the simulation as a compromise required a few changes to the circuit and code, as the pins used were different in the final setup compared to the simulated one.

\begin{figure} [!ht]
	\centering
	\includegraphics[scale=0.3]{blueprint.png}
	\caption{Blueprint of the circuit.}
	\label{fig:blueprint}
\end{figure}

The schematic (see Figure \ref{fig:sch_noPin}) shows the basic implementation of a pushbutton with an LED that will light up when the button is pressed. The power goes from the arduino with a voltage of 5 V to a resistor R1 connected to the button S1. The button is connected to a pin on the arduino as an input, that will read the state of the button. When the button is pressed down a connection is made to the LED D1 which will then light up. This was the schematic implemented for the note, beat and undo buttons, where they are connected in parallel.

\begin{figure} [!ht]
	\centering
	\includegraphics[scale=0.3]{sch_noPin.png}
	\caption{Schematic of button with LED without pin for LED.}
	\label{fig:sch_noPin}
\end{figure}
\par 

The schematic in Figure \ref{fig:sch_pin} shows the basic setup for a pushbutton with a LED connected to a pin on the arduino. The current flows through R1 and goes to the push button S1 which is set to a pin again, where it reads the value of the button. When a connection is made, the button establishes a connection to ground (GND). The LED gets current from the pin which is set to output 5 V. The current travels through R2 to the LED D1 which then leads to ground. This schematic was implemented for the chord buttons where they are put in a parallel circuit. The LEDs have each their own circuit.

 \begin{figure} [!ht]
	\centering
	\includegraphics[scale=0.3]{sch_pin.png}
	\caption{Schematic of button with LED with pin for LED}
	\label{fig:sch_pin}
\end{figure}

\par 

The last schematic (see Figure \ref{fig:sch_pot}) shows the circuit for a linear potentiometer and four LEDs. The four LEDs are each connected to an output pin on the arduino with a resistor before going to the LED, leading to ground. The current for the potentiometer comes from the 5 V pin and goes through R1 to the potentiometer Rpot1 and then to ground. The middle pin of the potentiometer is connected to the analog pin which reads the resistance of the potentiometer. This setup is used to control the choosing of waveforms, based on the resistance of the potentiometer.

 \begin{figure} [!ht]
	\centering
	\includegraphics[scale=0.3]{sch_pot.png}
	\caption{Schematic of potentiometer with LEDs connected to pins on the arduino}
	\label{fig:sch_pot}
\end{figure}

\section{Minimum Implementation Requirements}
The spatial requirements for the minimum implementation is to have the system in somewhat calm and isolated environment without too many distractions. The system can be run on a laptop with connected Arduino and at least one breadboard with all the components attached. For a better interaction for the user, it would be optimal if the system was soldered and all the wiring hidden in a box, so that only the interactive parts, such as buttons, are visible. The user can utilise either headphones or loudspeakers, depending on the loudness of environment.
\par
Specifically for the system, there should be a minimum of five notes implemented, so that there is a bit of a variety that the user can manipulate with. The waveforms should be presented and preset. Therefore, the customization is not a requirement. The solar system will stand on its own with no other galaxies. In other words, other tracks cannot be stored and played later, apart from the current one. 
